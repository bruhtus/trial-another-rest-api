export const get_controller = (req, res) => {
  return res.json({ message: 'hello world' })
}

export const post_controller = (req, res) => {
  return res.json(req.body)
}

export const put_controller = (req, res) => {
  return res.json(req.body)
}

export const delete_controller = (req, res) => {
  return res.json(req.body)
}
