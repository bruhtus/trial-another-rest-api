import 'regenerator-runtime/runtime'
import request from 'supertest'
import app from '../server'

describe('check route exist', () => {
  test('get route /api exist', async () => {
    const res = await request(app)
      .get('/api')
    expect(res.statusCode).toBe(200)
    expect(res.body.message).toBe('hello world')
  })

  test('post route /api exist', async () => {
    const res = await request(app)
      .post('/api')
      .send({ something: 'big' })
    expect(res.statusCode).toBe(200)
    expect(res.body.something).toBe('big')
  })

  test('put route /api exist', async () => {
    const res = await request(app)
      .put('/api')
      .send({ something: 'big' })
    expect(res.statusCode).toBe(200)
    expect(res.body.something).toBe('big')
  })

  test('delete route /api exist', async () => {
    const res = await request(app)
      .delete('/api')
      .send({ something: 'big' })
      .set('something', 'big')
    expect(res.statusCode).toBe(200)
    expect(res.body.something).toBe('big')
  })
})
