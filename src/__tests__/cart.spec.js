// import * as cart from '../cart'
import { addToCart, items } from '../cart'

describe('check cart file', () => {
  test('addToCart able to add item', () => {
    const input = [
      'big',
      'small'
    ]

    input.forEach(item => {
      // cart.addToCart(item)
      addToCart(item)
    })

    // expect(cart.items.length).toBe(2)
    expect(items.length).toBe(2)
  })
})
