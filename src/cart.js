const items = []

const addToCart = (item) => {
  items.push(item)
}

// addToCart('big')
// addToCart('nope')
// console.log(items)

// export default { addToCart, items }
export { addToCart, items }
