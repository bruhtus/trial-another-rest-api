import express from 'express'
import morgan from 'morgan'
import { urlencoded, json } from 'body-parser'

import {
  get_controller,
  post_controller,
  put_controller,
  delete_controller,
} from './server.controllers'

const app = express()

app.use(urlencoded({ extended: true }))
app.use(json())
app.use(morgan('dev'))

// app.get('/api', get_controllers())
app.get('/api', get_controller)
app.post('/api', post_controller)
app.put('/api', put_controller)
app.delete('/api', delete_controller)

export default app

app.listen(8080, () => {
  console.log('server on')
})
