"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.items = exports.addToCart = void 0;
var items = [];
exports.items = items;

var addToCart = function addToCart(item) {
  items.push(item);
}; // addToCart('big')
// addToCart('nope')
// console.log(items)
// export default { addToCart, items }


exports.addToCart = addToCart;