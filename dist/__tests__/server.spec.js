"use strict";

require("regenerator-runtime/runtime");

var _supertest = _interopRequireDefault(require("supertest"));

var _server = _interopRequireDefault(require("../server"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

describe('check route exist', function () {
  test('get route /api exist', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var res;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _supertest["default"])(_server["default"]).get('/api');

          case 2:
            res = _context.sent;
            expect(res.statusCode).toBe(200);
            expect(res.body.message).toBe('hello world');

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));
  test('post route /api exist', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var res;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return (0, _supertest["default"])(_server["default"]).post('/api').send({
              something: 'big'
            });

          case 2:
            res = _context2.sent;
            expect(res.statusCode).toBe(200);
            expect(res.body.something).toBe('big');

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  })));
  test('put route /api exist', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    var res;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _supertest["default"])(_server["default"]).put('/api').send({
              something: 'big'
            });

          case 2:
            res = _context3.sent;
            expect(res.statusCode).toBe(200);
            expect(res.body.something).toBe('big');

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  })));
  test('delete route /api exist', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
    var res;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return (0, _supertest["default"])(_server["default"])["delete"]('/api').send({
              something: 'big'
            }).set('something', 'big');

          case 2:
            res = _context4.sent;
            expect(res.statusCode).toBe(200);
            expect(res.body.something).toBe('big');

          case 5:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  })));
});