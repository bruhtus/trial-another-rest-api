"use strict";

var _cart = require("../cart");

// import * as cart from '../cart'
describe('check cart file', function () {
  test('addToCart able to add item', function () {
    var input = ['big', 'small'];
    input.forEach(function (item) {
      // cart.addToCart(item)
      (0, _cart.addToCart)(item);
    }); // expect(cart.items.length).toBe(2)

    expect(_cart.items.length).toBe(2);
  });
});