"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _morgan = _interopRequireDefault(require("morgan"));

var _bodyParser = require("body-parser");

var _server = require("./server.controllers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])();
app.use((0, _bodyParser.urlencoded)({
  extended: true
}));
app.use((0, _bodyParser.json)());
app.use((0, _morgan["default"])('dev')); // app.get('/api', get_controllers())

app.get('/api', _server.get_controller);
app.post('/api', _server.post_controller);
app.put('/api', _server.put_controller);
app["delete"]('/api', _server.delete_controller);
var _default = app;
exports["default"] = _default;
app.listen(8080, function () {
  console.log('server on');
});