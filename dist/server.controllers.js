"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.put_controller = exports.post_controller = exports.get_controller = exports.delete_controller = void 0;

var get_controller = function get_controller(req, res) {
  return res.json({
    message: 'hello world'
  });
};

exports.get_controller = get_controller;

var post_controller = function post_controller(req, res) {
  return res.json(req.body);
};

exports.post_controller = post_controller;

var put_controller = function put_controller(req, res) {
  return res.json(req.body);
};

exports.put_controller = put_controller;

var delete_controller = function delete_controller(req, res) {
  return res.json(req.body);
};

exports.delete_controller = delete_controller;